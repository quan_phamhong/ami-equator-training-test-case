*** Settings ***
Library           RequestsLibrary

*** Variables ***
${base_url}  http://localhost:8081/

*** keywords ***


*** Test Cases ***
TC_001_Get_Request - get allEmployer
    create session  Get_All_Employer   ${base_url}
    ${response} =  get request  Get_All_Employer  employer/getAllEmployer
    ${code}=  convert to string  ${response.status_code}
    should be equal  ${code}  200
    log to console  ${response.status_code}
    log to console  ${response.content}