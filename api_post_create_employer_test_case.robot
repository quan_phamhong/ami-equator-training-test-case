*** Settings ***
Library           RequestsLibrary
Library           JSONLibrary
Library           Collections


*** Variables ***
${base_url}  http://localhost:8081/
${create_employer_path}  employer/createEmployer

*** keywords ***


*** Test Cases ***
TC_002_Create_Employer - create Employer
    create session          api     ${base_url}     disable_warnings=1
    ${data}    catenate    {"name": "test_create_dddef", "mobileNumber": "0394495595856", "email": "phquan@gmail.com" }
    ${header}   create dictionary    content-type=application/json
    ${response}  post request  api  ${create_employer_path}  data=${data}  headers=${header}
    ${code}=  convert to string  ${response.status_code}
    log  ${data}
    log   ${response.text}
    should be equal  ${code}  200
