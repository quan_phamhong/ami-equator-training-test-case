*** Settings ***
Library           RequestsLibrary
Library           JSONLibrary
Library           Collections


*** Variables ***
${base_url}  http://localhost:8081/
${id}  1
${delete_employer__byId_path}  employer/deleteEmployerById/${id}


*** keywords ***


*** Test Cases ***
TC_004_Delete_Employer_ById - delete Employer byId
    create session          api     ${base_url}     disable_warnings=1
    ${header}   create dictionary    content-type=application/json
    ${response}  post request  api  ${delete_employer__byId_path}  headers=${header}
    ${code}=  convert to string  ${response.status_code}
    log   ${response.text}
    should be equal  ${code}  200
